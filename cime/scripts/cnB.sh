#! /bin/csh -f

set mach=cori-knl
set compset=BC51D
set res=f02_g16 

set wdir="/global/cscratch1/sd/ndk/acme_scratch/cori-knl"

#set cn="$wdir/testcesm155-n094np6266m68"
#set cn="$wdir/testcesm155-n020np1350t01m68b"
set cn="$wdir/testcesm155.$compset.$res.n020np1350m68t01"
echo "cn=$cn"

#-compset BC51D -res f02_g16
#-compset FAMIPC5 -res ne30np4_ne30np4
create_newcase -case $cn -res $res -mach $mach -compiler intel -compset $compset  >& cnlog.txt

set nodes=20
set np=1350
set nt=1

set cores_per_node=68  # 64 or 68
@ mpn = ($np / $nodes) 
@ pes_per_node = ((4 * $cores_per_node))


cd $cn

./xmlchange -file env_run.xml REST_OPTION="none"
./xmlchange -file env_run.xml PIO_STRIDE=$cores_per_node
#./xmlchange -file env_run.xml PIO_NUM_IOTASKS=

./xmlchange -file env_mach_pes.xml MAX_TASKS_PER_NODE=$cores_per_node
./xmlchange -file env_mach_pes.xml PES_PER_NODE=$pes_per_node

./xmlchange -file env_mach_pes.xml NTASKS_ATM="$np"
./xmlchange -file env_mach_pes.xml NTASKS_LND="$np"
./xmlchange -file env_mach_pes.xml NTASKS_ICE="$np"
./xmlchange -file env_mach_pes.xml NTASKS_OCN="$np"
./xmlchange -file env_mach_pes.xml NTASKS_CPL="$np"
./xmlchange -file env_mach_pes.xml NTASKS_GLC="$np"
./xmlchange -file env_mach_pes.xml NTASKS_ROF="$np"
./xmlchange -file env_mach_pes.xml NTASKS_WAV="$np"

./xmlchange -file env_mach_pes.xml NTHRDS_ATM="$nt"
./xmlchange -file env_mach_pes.xml NTHRDS_LND="$nt"
./xmlchange -file env_mach_pes.xml NTHRDS_ICE="$nt"
./xmlchange -file env_mach_pes.xml NTHRDS_OCN="$nt"
./xmlchange -file env_mach_pes.xml NTHRDS_CPL="$nt"
./xmlchange -file env_mach_pes.xml NTHRDS_GLC="$nt"
./xmlchange -file env_mach_pes.xml NTHRDS_ROF="$nt"
./xmlchange -file env_mach_pes.xml NTHRDS_WAV="$nt"
pwd

./xmlchange  -file env_batch.xml JOB_QUEUE="regular"
./xmlchange  -file env_batch.xml JOB_WALLCLOCK_TIME="01:30:00"

# turn off short-term archiving which auto-submits a second job
./xmlchange  -file env_run.xml DOUT_S="FALSE"

./xmlchange -file env_build.xml GMAKE_J="12"
#./xmlchange -file env_build.xml DEBUG=TRUE

echo " setting up"
case.setup >& csout.txt

echo " building"
case.build >& buildout.txt
echo " submitting"
case.submit >& submitout.txt

echo " done"
